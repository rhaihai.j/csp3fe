import React, { useState, useEffect } from "react"
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Products from './pages/Products';
import Specific from './pages/Specific';
import MyCart from './pages/MyCart';
import Orders from './pages/Orders';
import Error from './pages/ErrorPage';

import "bootswatch/dist/cosmo/bootstrap.min.css"
import './App.css';
import {UserProvider} from "./UserContext"

function App() {

  const[user, setUser]=useState({
    id:null,
    isAdmin:null,
    email:null
  })

  useEffect(()=>{
    //console.log(process.env.REACT_APP_API_URL)
    fetch(`${process.env.REACT_APP_API_URL}/users/view-details`,{
      method:"GET",
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
    }).then(res=>res.json()).then(data=>{
      console.log(data)
      if(typeof data._id !== "undefined"){
        setUser({
          id:data._id,
          isAdmin:data.isAdmin,
          email:data.email
        })
      }
      else{
        setUser({
          id:null,
          isAdmin:null,
          email:null
        })
      }
    })
  },[])

   const unsetUser = () => {
   localStorage.clear() 
  }


  return(
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar />
        <Routes>
           <Route path="/" element={<Home/>}/>
           <Route path="/register" element={<Register/>}/>
           <Route path="/login" element={<Login/>}/>
           <Route path="/products" element={<Products/>}/>
           <Route path="/products/:productId" element={<Specific/>}/>
           <Route path="/cart" element={<MyCart/>}/>
           <Route path="/orders" element={<Orders/>}/>
           <Route path="/logout" element={<Logout/>}/>
           <Route element={<Error/>}/>
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
